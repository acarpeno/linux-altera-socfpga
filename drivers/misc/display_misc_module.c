/*
 * Copyright (C) 2015 Altera Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/io.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/miscdevice.h>
#include <linux/sched.h>
#include <linux/slab.h>

/* defines */
#define NAME_BUF_SIZE	(32)

/* globals */
static struct list_head g_dev_list;
static struct semaphore g_dev_list_sem;
static int g_dev_index;

static u8 binto7seg[16]={0x40,0x79,0x24,0x30,0x19,0x12,0x02,0x78,0x00,0x18,0x08,0x03,0x46,0x21,0x06,0x0E};


/* device structure */
struct display_dev {
	char name[NAME_BUF_SIZE];
	char out_region_name[NAME_BUF_SIZE];
	struct list_head dev_list;
	struct semaphore dev_sem;
	wait_queue_head_t wait_queue;
	struct resource *out_res;
	void __iomem *ioremap_out_addr;
	uint32_t open;
	struct miscdevice miscdev;
};

static ssize_t write_to_disp(u8 mode, u8 disp_num, u8 data){
	struct display_dev *the_display_dev = NULL;
	struct list_head *next_list_entry;
	int found_it = 0;

	list_for_each(next_list_entry, &g_dev_list) {
		the_display_dev = list_entry(next_list_entry,
					       struct display_dev, dev_list);
		if ( ((the_display_dev->out_res->start & 0x000000F0)>>4) == disp_num) {
			found_it = 1;
			break;
		}
	}

	if (found_it == 0)
		return -ENXIO;

	// Binary value in data
	if (mode == 1){
		if (data > 15) {
			pr_err("Invalid display data %d\n", data);
			return -EINVAL;
		}
		iowrite8(binto7seg[data], the_display_dev->ioremap_out_addr);
	}

	// Direct seven segment code in data
	else iowrite8(data, the_display_dev->ioremap_out_addr);

	return 0;
}


/* misc device - display */
static ssize_t display_write(struct file *fp,
			       const char __user *user_buffer, size_t count,
			       loff_t *offset)
{
	struct display_dev *the_display_dev = fp->private_data;
	char buf[10];
	u8 data;

	if (down_interruptible(&the_display_dev->dev_sem)) {
		pr_info("display_write sem interrupted exit\n");
		return -ERESTARTSYS;
	}

	if (copy_from_user(&buf[0], user_buffer, count)) {
		up(&the_display_dev->dev_sem);
		pr_info("display_write copy_to_user exit\n");
		return -EFAULT;
	}
	buf[count]=0;

	if (kstrtou16(buf, 16, (u16 *)&data) < 0) {
		pr_err("Could not convert string to integer\n");
		return -EINVAL;
	}

	if (data > 15) {
		pr_err("Invalid display data %d\n", data);
		return -EINVAL;
	}

	iowrite8(binto7seg[data], the_display_dev->ioremap_out_addr);


	up(&the_display_dev->dev_sem);
	wake_up_interruptible(&the_display_dev->wait_queue);

	return count;
}


static int display_open(struct inode *ip, struct file *fp)
{
	struct display_dev *the_display_dev = NULL;
	uint32_t this_minor;

	struct list_head *next_list_entry;
	int found_it = 0;

	if (down_interruptible(&g_dev_list_sem))
		return -ERESTARTSYS;

	this_minor = iminor(ip);

	list_for_each(next_list_entry, &g_dev_list) {
		the_display_dev = list_entry(next_list_entry,
					       struct display_dev, dev_list);
		if (the_display_dev->miscdev.minor == this_minor) {
			found_it = 1;
			break;
		}
	}

	up(&g_dev_list_sem);

	if (found_it == 0)
		return -ENXIO;

	fp->private_data = the_display_dev;

	return 0;
}

static int display_release(struct inode *ip, struct file *fp)
{
	if (down_interruptible(&g_dev_list_sem))
		return -ERESTARTSYS;

	// Don't doing anything right now. But in the future, who knows?

	up(&g_dev_list_sem);

	return 0;
}

static const struct file_operations display_fops = {
	.owner = THIS_MODULE,
	.open = display_open,
	.release = display_release,
//	.read = unused,
	.write = display_write,
};

/* platform driver */
static int platform_probe(struct platform_device *pdev)
{
	int ret_val;
	struct resource *r0 = NULL;
	struct resource *temp_res = NULL;
	struct display_dev *the_display_dev;

	if (down_interruptible(&g_dev_list_sem))
		return -ERESTARTSYS;

	ret_val = -ENOMEM;

	/* allocate a display_dev structure */
	the_display_dev = kzalloc(sizeof(struct display_dev), GFP_KERNEL);
	if (the_display_dev == NULL) {
		pr_err("kzalloc failed\n");
		goto bad_exit_return;
	}
	/* initialize the display_dev structure */
	scnprintf(the_display_dev->name, NAME_BUF_SIZE, "display_%d",
		  g_dev_index);

	INIT_LIST_HEAD(&the_display_dev->dev_list);
	sema_init(&the_display_dev->dev_sem, 1);
	init_waitqueue_head(&the_display_dev->wait_queue);

	the_display_dev->out_res = NULL;
	the_display_dev->ioremap_out_addr = NULL;
	the_display_dev->open = 0;
	the_display_dev->miscdev.minor = MISC_DYNAMIC_MINOR;
	the_display_dev->miscdev.name = the_display_dev->name;
	the_display_dev->miscdev.fops = &display_fops, ret_val = -EINVAL;

	/* get our three expected memory resources */
	r0 = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (r0 == NULL) {
		pr_err("IORESOURCE_MEM, 0 does not exist\n");
		goto bad_exit_kfree_the_display_dev;
	}
	the_display_dev->out_res = r0;

	ret_val = -EBUSY;

	/* reserve our memory regions */
	temp_res = request_mem_region(the_display_dev->out_res->start,
				      resource_size(the_display_dev->out_res),
				      strncat(strncpy
					      (the_display_dev->
					       out_region_name,
					       pdev->name, NAME_BUF_SIZE),
					      ".out", NAME_BUF_SIZE));
	if (temp_res == NULL) {
		pr_err("request_mem_region failed: \"out\"\n");
		goto bad_exit_kfree_the_display_dev;
	}

	/* ioremap our memory regions */
	the_display_dev->ioremap_out_addr =
	    ioremap(the_display_dev->out_res->start,
		    resource_size(the_display_dev->out_res));
	if (the_display_dev->ioremap_out_addr == NULL) {
		pr_err("ioremap failed: ioremap_out_addr\n");
		goto bad_exit_release_mem_region_out;
	}

	ret_val = -EIO;

	/* register misc device display */
	ret_val = misc_register(&the_display_dev->miscdev);
	if (ret_val != 0) {
		pr_warn("Could not register device \"%s\"...",
			the_display_dev->name);
		goto bad_exit_iounmap_out;
	}

	/* clean up and exit */
	list_add(&the_display_dev->dev_list, &g_dev_list);
	g_dev_index++;
	platform_set_drvdata(pdev, the_display_dev);
	up(&g_dev_list_sem);

	pr_info("%s module successfully connected\n", the_display_dev->name);
	return 0;

bad_exit_iounmap_out:
	iounmap(the_display_dev->ioremap_out_addr);
bad_exit_release_mem_region_out:
	release_mem_region(the_display_dev->out_res->start,
			   resource_size(the_display_dev->out_res));
bad_exit_kfree_the_display_dev:
	kfree(the_display_dev);
bad_exit_return:
	up(&g_dev_list_sem);
	return ret_val;
}

static int platform_remove(struct platform_device *pdev)
{
	struct display_dev *the_display_dev;
	char name[NAME_BUF_SIZE];

	if (down_interruptible(&g_dev_list_sem))
		return -ERESTARTSYS;

	the_display_dev = platform_get_drvdata(pdev);

	strcpy(name, the_display_dev->name);

	list_del_init(&the_display_dev->dev_list);

	misc_deregister(&the_display_dev->miscdev);
	iounmap(the_display_dev->ioremap_out_addr);
	release_mem_region(the_display_dev->out_res->start,
			   resource_size(the_display_dev->out_res));
	kfree(the_display_dev);

	up(&g_dev_list_sem);
	pr_info("%s module successfully removed\n", name);
	return 0;
}

static struct of_device_id display_driver_dt_ids[] = {
	{
	 .compatible = "display,driver-1.0"},
	{ /* end of table */ }
};

MODULE_DEVICE_TABLE(of, display_driver_dt_ids);

static struct platform_driver the_platform_driver = {
	.probe = platform_probe,
	.remove = platform_remove,
	.driver = {
		   .name = "display_driver",
		   .owner = THIS_MODULE,
		   .of_match_table = display_driver_dt_ids,
		   },
};

static int display_init(void)
{
	int ret_val;

	INIT_LIST_HEAD(&g_dev_list);
	sema_init(&g_dev_list_sem, 1);
	g_dev_index = 0;

	ret_val = platform_driver_register(&the_platform_driver);
	if (ret_val != 0) {
		pr_err("platform_driver_register returned %d\n", ret_val);
		return ret_val;
	}

	return 0;
}

static void display_exit(void)
{
	platform_driver_unregister(&the_platform_driver);
}

module_init(display_init);
module_exit(display_exit);

EXPORT_SYMBOL(write_to_disp);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Antonio Carpeño <antonio.cruiz@upm.es>");
MODULE_AUTHOR("Mariano Ruiz <mariano.ruiz@upm.es>");
MODULE_DESCRIPTION("Display controller module - multiple device instances");
MODULE_VERSION("1.0");
