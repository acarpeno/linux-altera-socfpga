
//#include <io.h>
#define SYSTEM_BUS_WIDTH 						(16)
#define __IO_CALC_ADDRESS_NATIVE(BASE, REGNUM)	((void *)(((unsigned char*)BASE) + ((REGNUM) * (SYSTEM_BUS_WIDTH/8))))
#define IORD(BASE, REGNUM) 						*(unsigned long*)(__IO_CALC_ADDRESS_NATIVE ((BASE), (REGNUM)))
#define IOWR(BASE, REGNUM, DATA) 				*(unsigned long*)(__IO_CALC_ADDRESS_NATIVE ((BASE), (REGNUM))) = (DATA)

// CONFIG ADC register
#define ADC_CONFIG_REG              0
#define IOADDR_ADC_CONFIG(base) 	__IO_CALC_ADDRESS_NATIVE(base, ADC_CONFIG_REG)
#define IOWR_ADC_CONFIG(base, data)	IOWR(base, ADC_CONFIG_REG, data)
#define ADC_CONFIG_MSK				(0xFFFF)

// acquisition mode
#define ADC_ADQ_MSK					(0x0010)

//Flags
#define ADC_FLAGS_MSK				(0xF8FF)

//BLOCK SIZE register
#define ADC_BS_REG              	1
#define IOADDR_ADC_BS(base) 		__IO_CALC_ADDRESS_NATIVE(base, ADC_BS_REG)
#define IOWR_ADC_BS(base, data) 	IOWR(base, ADC_BS_REG, data)
#define ADC_BLOCK_SIZE_MSK			(0xF000)

//SAMPLE VALUE register
#define ADC_SV_REG              	1
#define IOADDR_ADC_SV(base) 		__IO_CALC_ADDRESS_NATIVE(base, ADC_SV_REG)
#define IOWR_ADC_SV(base, data) 	IOWR(base, ADC_SV_REG, data)
#define ADC_SAMPLE_VALUE_MSK		(0xF000)

/* STATUS register */
#define ADC_STATUS_REG              0
#define IOADDR_ADC_STATUS(base) 	__IO_CALC_ADDRESS_NATIVE(base, ADC_STATUS_REG)
#define IORD_ADC_STATUS(base) 		IORD(base, ADC_STATUS_REG)

/*
 * ioctl values
 */
#define OUR_IOC_TYPE		(0xEE)
#define IOC_ADQ_MODE		_IOW(OUR_IOC_TYPE, 0, u8)
#define IOC_PRINT_DISP		_IOW(OUR_IOC_TYPE, 1, u16)
#define IOC_CLEAR_FLAGS		_IO(OUR_IOC_TYPE, 2)
#define IOC_WAIT_ACQDONE	_IOR(OUR_IOC_TYPE, 3, u16)

/*
 * ioctl values
 *
#define IOC_ADQ_MODE		(0x4001EE00)
#define IOC_PRINT_DISP		(0x4002EE01)
#define IOC_CLEAR_FLAGS		(0x0000EE02)
#define IOC_WAIT_ACQDONE   	(0x8002EE03)
 */


/*To decode a hex IOCTL code:

Most architectures use this generic format, but check
include/ARCH/ioctl.h for specifics, e.g. powerpc
uses 3 bits to encode read/write and 13 bits for size.

 bits    meaning
 31-30	00 - no parameters: uses _IO macro
	10 - read: _IOR
	01 - write: _IOW
	11 - read/write: _IOWR

 29-16	size of arguments

 15-8	ascii character supposedly
	unique to each driver

 7-0	function #


So for example 0x82187201 is a read with arg length of 0x218,
character 'r' function 1. Grepping the source reveals this is:

#define VFAT_IOCTL_READDIR_BOTH         _IOR('r', 1, struct dirent [2])*/



