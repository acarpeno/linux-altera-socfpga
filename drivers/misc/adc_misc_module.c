#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/device.h>
#include <linux/platform_device.h>
#include <linux/uaccess.h>
#include <linux/ioport.h>
#include <linux/io.h>
#include <linux/clk.h>
#include <linux/miscdevice.h>
#include <linux/fs.h>
#include <linux/ioctl.h>
#include <linux/interrupt.h>
#include <linux/wait.h>
#include <linux/sched.h>
#include <linux/slab.h>

#include "adc_module.h"
#include "msgdma_descriptor_regs.h"
#include "msgdma_csr_regs.h"


extern ssize_t adc_dma_read_block(char *buffer, size_t count);
extern ssize_t write_to_disp(u8 mode, u8 disp_num, u8 data);

/* global variables */
static struct semaphore g_dev_probe_sem;
static int g_platform_probe_flag;
static void *adc_map_mem;
static int g_adc_driver_base_addr;
static int g_adc_driver_size;
static int g_adc_driver_irq;
static spinlock_t g_irq_lock;
static wait_queue_head_t g_irq_wait_queue;
static int interrupt_flag;
static struct platform_driver adc_platform_driver;

int num_int;

static int print_to_display(u8 mode, u16 data){
	u8 digit[6];
	u16 divisor, remainder;
	int i;

	// hexadecimal mode
	// 0: status
	if (mode == 0){
		write_to_disp(0, 5, 0x07); // "t"
		digit[3] = (data & 0xF000) >> 12;
		digit[2] = (data & 0x0F00) >> 8;
		digit[1] = (data & 0x00F0) >> 4;
		digit[0] = data & 0x000F;
	}
	// decimal mode.
	// 1: finished (print global average)
	// 2: running (print block average)
	// 3: single sample (print sample value)
	else {
		if (mode == 1) write_to_disp(0, 5, 0xAF); 			// "r"
		else if (mode == 2) write_to_disp(0, 5, 0x08); 		// "A"
		else write_to_disp(1, 5, 0x05); 		 			// "S"

		remainder = data;
		divisor=1000;
		for (i=3; i>=0; i--){
			digit[i] = (remainder/divisor);
			remainder %= divisor;
			divisor /= 10;
		}
	}


	write_to_disp(0, 4, 0x7F); //display off
	for (i=0; i<4;i++) {
		if (write_to_disp(1, i, digit[i]) != 0) {
			pr_info("adc_dev_print_to display error\n");
			return -ERESTARTSYS;
		}
	}

	return 0;
}

/******************************************************************************
		MISC DEVICE DRIVER
******************************************************************************/


/* misc device - adc_dev */

struct acq_params {
	u16 blocksize;
	u8 channel;
	u8 acq_mode;
	u8 st_mode;
	u8 sampling_rate;
	u8 test_mode;
	u8 eint;
	uint8_t start_acq;
	u16 num_blocks;
};

struct adc_dev {
	struct semaphore sem;
	struct acq_params params;
	uint32_t open;
};

static struct adc_dev the_adc_dev = {
	.params = {10,0,0,0,0,0,0,0,1}
};

static int wait_for_adc(u16 *status)
{
	unsigned long flags;

	// If the interrupts are set the function wait for that event to happen
	if ((the_adc_dev.params.eint & 0xC) == 0xC) {
		if (wait_event_interruptible(g_irq_wait_queue, interrupt_flag != 0)) {
			pr_info("adc_dev_read wait interrupted exit\n");
			return -ERESTARTSYS;
		}

		/* acquire the irq_lock */
		spin_lock_irqsave(&g_irq_lock, flags);

		interrupt_flag = 0;

		/* release the irq_lock */
		spin_unlock_irqrestore(&g_irq_lock, flags);
		*status = ioread16(IOADDR_ADC_STATUS(adc_map_mem));
	}
	// If the iterrupts are not set
	else {
		// Making polling till acquisition done or error condition
		*status = ioread16(IOADDR_ADC_STATUS(adc_map_mem));
		while ((*status & 0x700) == 0) *status = ioread16(IOADDR_ADC_STATUS(adc_map_mem));
	}

	return 0;
}


static int adc_dev_open(struct inode *ip, struct file *fp)
{
	struct adc_dev *dev = &the_adc_dev;
	uint32_t access_mode;

	if (down_interruptible(&dev->sem)) {
		pr_info("adc_dev_open sem interrupted exit\n");
		return -ERESTARTSYS;
	}

	access_mode = fp->f_flags & O_ACCMODE;
	switch (access_mode) {
	case (O_RDONLY):
		if (dev->open != 0)
			return -EBUSY;
		dev->open = 1;
		break;
	case (O_WRONLY):
		if (dev->open != 0)
			return -EBUSY;
		dev->open = 1;
		break;
	case (O_RDWR):
		if (dev->open != 0)
			return -EBUSY;
		dev->open = 1;
		break;
	default:
		return -EINVAL;
	}



	fp->private_data = dev;

	up(&dev->sem);
	return 0;
}



static int adc_dev_release(struct inode *ip, struct file *fp)
{
	struct adc_dev *dev = fp->private_data;

	uint32_t access_mode;

	access_mode = fp->f_flags & O_ACCMODE;
	switch (access_mode) {
	case (O_RDONLY):
		dev->open = 0;
		break;
	case (O_WRONLY):
		dev->open = 0;
		break;
	case (O_RDWR):
		dev->open = 0;
		break;
	default:
		return -EINVAL;
	}
	return 0;
}

static long adc_dev_ioctl(struct file *fp, unsigned int cmd, unsigned long arg)
{
	struct adc_dev *dev = fp->private_data;

	u16 current_config, data_disp;
	uint8_t acq_mode;
	unsigned long flags;

	if (down_interruptible(&dev->sem)) {
		pr_info("adc_dev_open sem interrupted exit\n");
		return -ERESTARTSYS;
	}

	switch (cmd) {

	case IOC_ADQ_MODE:

		if (get_user(acq_mode, (uint32_t *)arg) < 0) {
			up(&dev->sem);
			pr_info("adc_dev_ioctl get_user exit.\n");
			return -EFAULT;
		}
		if (acq_mode > 1) {
			up(&dev->sem);
			pr_err("Invalid adq_mode value %d\n", acq_mode);
			return -EINVAL;
		}

		/* acquire the irq_lock */
		spin_lock_irqsave(&g_irq_lock, flags);

		dev->params.acq_mode = acq_mode;

		current_config = ioread16(IOADDR_ADC_STATUS(adc_map_mem));
		switch (acq_mode){
		case 0:
			iowrite16(current_config & (~ADC_ADQ_MSK), IOADDR_ADC_CONFIG(adc_map_mem));
			break;
		case 1:
			iowrite16(current_config | ADC_ADQ_MSK, IOADDR_ADC_CONFIG(adc_map_mem));
			break;
		default:
			pr_info("adc_adq_mode bad cmd exit\n");
		}

		/* release the irq_lock */
		spin_unlock_irqrestore(&g_irq_lock, flags);

		break;

		case IOC_PRINT_DISP:


			if (get_user(data_disp, (uint32_t *)arg) < 0) {
				up(&dev->sem);
				pr_info("adc_dev_ioctl get_user exit.\n");
				return -EFAULT;
			}

			if (data_disp > 9999) {
				up(&dev->sem);
				pr_err("Invalid data_disp value %d\n", data_disp);
				return -EINVAL;
			}

			// If running "print 'r'". If not running "print 'A'"
			if (dev->params.acq_mode == 1) print_to_display(1, data_disp);
			else print_to_display(2, data_disp);

			break;

	case IOC_CLEAR_FLAGS:
		/* acquire the irq_lock */
		spin_lock_irqsave(&g_irq_lock, flags);

		current_config = ioread16(IOADDR_ADC_STATUS(adc_map_mem));
		iowrite16(current_config & ADC_FLAGS_MSK, IOADDR_ADC_CONFIG(adc_map_mem));

		/* release the irq_lock */
		spin_unlock_irqrestore(&g_irq_lock, flags);

		break;

	case IOC_WAIT_ACQDONE:
		if (wait_for_adc(&current_config) < 0){
			up(&dev->sem);
			pr_info("adc_dev_ioctl wait for acq done exit\n");
			return -EFAULT;
		}

		if (put_user(current_config, (uint32_t *)arg) < 0) {
			up(&dev->sem);
			pr_info("adc_dev_ioctl put_user exit\n");
			return -EFAULT;
		}

		break;

	default:
		up(&dev->sem);
		pr_info("adc_dev_ioctl bad cmd exit\n");
		return -EINVAL;
	}

	up(&dev->sem);
	return 0;
}

static ssize_t
adc_dev_read(struct file *fp, char __user *user_buffer, 
		     size_t count, loff_t *offset)
{
	struct adc_dev *dev = fp->private_data;

	u16 *sample;
	u16 increment = 0;
	size_t i, ret;
	u16 status;

	// Allocating memory to save the samples
	sample = kmalloc(count*2, GFP_KERNEL);

	if (sample == NULL) {
		pr_err("kmalloc failed\n");
		return -EFAULT;
	}

	// Wait until block acquisition finishes or an error appears
	if (wait_for_adc(&status) < 0){
		kfree(sample);
		return -ERESTARTSYS;
	}

	// Check errors and early return in case
	status = ioread16(IOADDR_ADC_STATUS(adc_map_mem));
	if ((status & 0x200) == 0x200) {
		pr_info("underrun error\n");
		kfree(sample);
		return -ERESTARTSYS;
	}
	else if ((status & 0x100) == 0x100) {
		pr_info("Overrun error\n");
		kfree(sample);
		return -ERESTARTSYS;
	}

	// Clearing flags
	iowrite16(status&0xF8FE, IOADDR_ADC_CONFIG(adc_map_mem));

	// Reading as many samples as specified by count parameter
	// If DMA mode is active
	if (dev->params.st_mode == 1){
		 ret = adc_dma_read_block((char *)sample, count);
	}
	// DMA mode is off
	else
		for (i = 0; i < count; i++){
			*(sample + increment) = ioread16(IOADDR_ADC_SV(adc_map_mem));
			increment += 1;
		}

	// Send samples to user space
	if (copy_to_user(user_buffer, sample, count*2)) {
		pr_info("adc_dev_read copy_to_user exit\n");
		return -EFAULT;
	}

	if (down_interruptible(&dev->sem)) {
		pr_info("blinker_dev_read sem interrupted exit\n");
		return -ERESTARTSYS;
	}

	// Actually. This is not necessary. We are not using this information
	dev->params.num_blocks--;

	up(&dev->sem);
	kfree(sample);
	return count;

}

static ssize_t
adc_dev_write(struct file *fp,
		   const char __user *user_buffer, size_t count,
		   loff_t *offset)
{
	struct adc_dev *dev = fp->private_data;
	u16 size;
	u16 config = 0;
	unsigned long flags;

	/* acquire the irq_lock */
	spin_lock_irqsave(&g_irq_lock, flags);

	interrupt_flag = 0;

	/* release the irq_lock */
	spin_unlock_irqrestore(&g_irq_lock, flags);

	if (down_interruptible(&dev->sem)) {
		pr_info("blinker_dev_read sem interrupted exit\n");
		return -ERESTARTSYS;
	}

	// Copy acquisition parameters from user space (acq_params struct)
	if (copy_from_user(&dev->params, user_buffer, count)) {
		pr_info("adc_dev_write copy_from_user exit\n");
		return -EFAULT;
	}

	up(&dev->sem);

	// Build config and blocksize info for adc registers
	size = dev->params.blocksize - 1;
	config = (dev->params.eint << 12) | (dev->params.test_mode << 11) |
			 (dev->params.sampling_rate << 6) |  (dev->params.st_mode << 5) |
			 (dev->params.acq_mode << 4) | (dev->params.channel << 1) | dev->params.start_acq;

	// Check acquisition parameters validity
	if (size > 4095) {
		pr_err("Invalid block_size value %d\n", size);
		return -EINVAL;
	}

	if (dev->params.channel > 7) {
		pr_err("Invalid channel value %d\n", dev->params.channel);
		return -EINVAL;
	}

	if (dev->params.sampling_rate > 7) {
		pr_err("Invalid sampling_rate value %d\n", dev->params.sampling_rate);
		return -EINVAL;
	}

	// Write the actual value into the registers
	iowrite16(size, IOADDR_ADC_BS(adc_map_mem));
	iowrite16(config, IOADDR_ADC_CONFIG(adc_map_mem));

	return count;
}

static const struct file_operations adc_dev_fops = {
	.owner = THIS_MODULE,
	.open = adc_dev_open,
	.release = adc_dev_release,
	.unlocked_ioctl = adc_dev_ioctl,
	.read = adc_dev_read,
	.write = adc_dev_write,
};


static struct miscdevice adc_dev_device = {
	.minor = MISC_DYNAMIC_MINOR,
	.name = "adc_misc",
	.fops = &adc_dev_fops,
};


/******************************************************************************
		IRQ HANDLER, SHOW AND STORE METHODS
******************************************************************************/

irqreturn_t adc_driver_interrupt_handler(int irq, void *dev_id)
{
	u16 data;

	spin_lock(&g_irq_lock);

	/* clear the interrupt by reading the state register*/
	data = ioread16(IOADDR_ADC_STATUS(adc_map_mem));

	interrupt_flag = 1;
	spin_unlock(&g_irq_lock);
	wake_up_interruptible(&g_irq_wait_queue);

	return IRQ_HANDLED;
}



ssize_t state_show(struct device_driver *drv, char *buf)
{
	u16 data;

	data = ioread16(IOADDR_ADC_STATUS(adc_map_mem));

	print_to_display(0, data);

	return scnprintf(buf, PAGE_SIZE,"%u\n", data);
}

ssize_t sample_show(struct device_driver *drv, char *buf)
{
	u16 data;

	data = ioread16(IOADDR_ADC_SV(adc_map_mem));

	print_to_display(3, data);

	return scnprintf(buf, PAGE_SIZE,"%u\n", data);
}

ssize_t block_size_store(struct device_driver *drv, const char *buf, size_t count)
{
	u16 data;
	unsigned long flags;

	if (buf == NULL) {
		pr_err("Error, string must not be NULL\n");
		return -EINVAL;
	}

	if (kstrtou16(buf, 10, &data) < 0) {
		pr_err("Could not convert string to integer\n");
		return -EINVAL;
	}

	if (data > 4095) {
		pr_err("Invalid configuration data %d\n", data);
		return -EINVAL;
	}

	/* acquire the irq_lock */
	spin_lock_irqsave(&g_irq_lock, flags);

	iowrite16(data, IOADDR_ADC_BS(adc_map_mem));

	/* release the irq_lock */
	spin_unlock_irqrestore(&g_irq_lock, flags);

	return count;
}

ssize_t config_store(struct device_driver *drv, const char *buf, size_t count)
{
	u16 data;
	unsigned long flags;

	if (buf == NULL) {
		pr_err("Error, string must not be NULL\n");
		return -EINVAL;
	}

	if (kstrtou16(buf, 16, &data) < 0) {
		pr_err("Could not convert string to integer\n");
		return -EINVAL;
	}

	/* acquire the irq_lock */
	spin_lock_irqsave(&g_irq_lock, flags);

	iowrite16(data, IOADDR_ADC_CONFIG(adc_map_mem));

	/* release the irq_lock */
	spin_unlock_irqrestore(&g_irq_lock, flags);

	return count;
}


static DRIVER_ATTR(config, S_IWUGO, NULL, config_store);
static DRIVER_ATTR(block_size, S_IWUGO, NULL, block_size_store);
static DRIVER_ATTR(state, S_IRUGO, state_show, NULL);
static DRIVER_ATTR(sample, S_IRUGO, sample_show, NULL);


/******************************************************************************
		PROBE, REMOVE, RELEASE, INIT AND EXIT METHODS
******************************************************************************/


static struct of_device_id adc_driver_dt_ids[] = {
	{
	 .compatible = "adc,driver-1.0"},
	{ /* end of table */ }
};

MODULE_DEVICE_TABLE(of, adc_driver_dt_ids);


static int adc_probe(struct platform_device *pdev)
{
	int ret = -EINVAL;
	struct resource *res;
	struct resource *adc_driver_mem_region;
	int irq;


	/* acquire the probe lock */
	if (down_interruptible(&g_dev_probe_sem))
		return -ERESTARTSYS;

	/* check that any other device is using the driver */
	if (g_platform_probe_flag != 0)
		goto bad_exit_return;

	/* get adc memory resource */
	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (res == NULL) {
		pr_err("IORESOURCE_MEM, 0 does not exist\n");
		goto bad_exit_return;
	}
	g_adc_driver_base_addr = res->start;
	g_adc_driver_size = resource_size(res);

	/* get our interrupt resource */
	irq = platform_get_irq(pdev, 0);
	if (irq < 0) {
		pr_err("irq not available\n");
		goto bad_exit_return;
	}
	g_adc_driver_irq = irq;

	/* register our interrupt handler */
	init_waitqueue_head(&g_irq_wait_queue);
	spin_lock_init(&g_irq_lock);
	interrupt_flag = 0;
	ret = request_irq(g_adc_driver_irq,
			      adc_driver_interrupt_handler,
			      0,
			      adc_platform_driver.driver.name,
			      &adc_platform_driver);
	if (ret) {
		pr_err("request_irq failed");
		goto bad_exit_return;
	}

	/* create the sysfs entries */
	ret = driver_create_file(&adc_platform_driver.driver, &driver_attr_block_size);
	if (ret < 0) {
		pr_err("failed to create block size sysf entry.");
		goto bad_exit_create_block_size_file;
	}

	ret = driver_create_file(&adc_platform_driver.driver, &driver_attr_config);
	if (ret < 0) {
		pr_err("failed to create config sysf entry.");
		goto bad_exit_create_config_file;
	}

	ret = driver_create_file(&adc_platform_driver.driver, &driver_attr_state);
	if (ret < 0) {
		pr_err("failed to create state sysf entry.");
		goto bad_exit_create_state_file;
	}

	ret = driver_create_file(&adc_platform_driver.driver, &driver_attr_sample);
	if (ret < 0) {
		pr_err("failed to create sample sysf entry.");
		goto bad_exit_create_sample_file;
	}

	/* reserve adc memory region */
	adc_driver_mem_region = request_mem_region(g_adc_driver_base_addr, g_adc_driver_size, "adc");
	if (adc_driver_mem_region == NULL) {
		pr_err("request_mem_region failed.");
		goto bad_exit_request_mem;
		ret = -EBUSY;
	}

	/* ioremap adc memory region */
	adc_map_mem = ioremap(g_adc_driver_base_addr, g_adc_driver_size);
	if (adc_map_mem == NULL) {
		pr_err("ioremap failed.");
		goto bad_exit_ioremap;
		ret = -EFAULT;
	}

	/* register misc device adc */
	sema_init(&the_adc_dev.sem, 1);
	ret = misc_register(&adc_dev_device);
	if (ret != 0) {
		pr_warn("Could not register misc device \"adc_misc\"...");
		goto bad_exit_register_misc_device;
	}

	/* mark the driver as being used*/
	g_platform_probe_flag = 1;

	/* release the semaphore */ 
	up(&g_dev_probe_sem);

	pr_info("adc device successfully connected\n");
	return 0;

	/* Exit with errors release the blocked resources */
bad_exit_register_misc_device:
	iounmap(adc_map_mem);
bad_exit_ioremap:
	release_mem_region(g_adc_driver_base_addr, g_adc_driver_size);
bad_exit_request_mem:
	driver_remove_file(&adc_platform_driver.driver, &driver_attr_sample);
bad_exit_create_sample_file:
	driver_remove_file(&adc_platform_driver.driver, &driver_attr_state);
bad_exit_create_state_file:
	driver_remove_file(&adc_platform_driver.driver, &driver_attr_config);
bad_exit_create_config_file:
	driver_remove_file(&adc_platform_driver.driver, &driver_attr_block_size);
bad_exit_create_block_size_file:
	free_irq(g_adc_driver_irq, &adc_platform_driver);
bad_exit_return:

	/* release the semaphore */ 
	up(&g_dev_probe_sem);
	pr_err("adc device connect FAILED");
	return ret;
}

static int adc_remove(struct platform_device *pdev)
{
	free_irq(g_adc_driver_irq, &adc_platform_driver);
	misc_deregister(&adc_dev_device);
	driver_remove_file(&adc_platform_driver.driver, &driver_attr_block_size);
	driver_remove_file(&adc_platform_driver.driver, &driver_attr_config);
	driver_remove_file(&adc_platform_driver.driver, &driver_attr_state);
	driver_remove_file(&adc_platform_driver.driver, &driver_attr_sample);
	release_mem_region(g_adc_driver_base_addr, g_adc_driver_size);
	iounmap(adc_map_mem);

	if (down_interruptible(&g_dev_probe_sem))
		return -ERESTARTSYS;

	/* mark the driver as free*/
	g_platform_probe_flag = 0;

	up(&g_dev_probe_sem);

	pr_info("adc device successfully removed\n");

	return 0;
}

static struct platform_driver adc_platform_driver = {
	.probe = adc_probe,
	.remove = adc_remove,
	.driver = {
		   .name = "adc_pd",
		   .owner = THIS_MODULE,
		   .of_match_table = adc_driver_dt_ids,
		   },
	
/*	   .shutdown = unused,
	   .suspend = unused,
	   .resume = unused,
	   .id_table = unused,
	 */
};


static int __init adc_init(void)
{
	int ret;

	sema_init(&g_dev_probe_sem, 1);

	ret = platform_driver_register(&adc_platform_driver);
	if (ret != 0) {
		pr_err("platform_driver_register returned %d\n", ret);
		goto bad_exit_driver_register;
	}

	pr_info("adc module successfully inserted\n");
	return 0;

bad_exit_driver_register:

	pr_err("adc module insert FAILED");
	return ret;

}

static void __exit adc_exit(void)
{
	platform_driver_unregister(&adc_platform_driver);

	pr_info("adc module successfully removed\n");

}

module_init(adc_init);
module_exit(adc_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Antonio Carpeño <antonio.cruiz@upm.es>");
MODULE_AUTHOR("Mariano Ruiz <mariano.ruiz@upm.es>");
MODULE_DESCRIPTION("adc peripheral misc_platform_driver example");
MODULE_VERSION("1.0");
