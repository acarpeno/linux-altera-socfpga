#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/clk.h>
#include <linux/io.h>
#include <linux/interrupt.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/miscdevice.h>
#include <linux/mm.h>
#include <linux/wait.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/dma-mapping.h>

#include "msgdma_module.h"
#include "msgdma_descriptor_regs.h"
#include "msgdma_csr_regs.h"

/* prototypes */
static struct platform_driver the_platform_driver;

/* globals */
static struct semaphore g_dev_probe_sem;
static int g_platform_probe_flag;
static int g_msgdma_csr_addr;
static int g_msgdma_csr_size;
static int g_msgdma_desc_addr;
static int g_msgdma_desc_size;
static int g_msgdma_irq;
static void *g_ioremap_csr_addr;
static void *g_ioremap_desc_addr;
static spinlock_t g_irq_lock;
static wait_queue_head_t g_irq_wait_queue;
static uint32_t g_irq_count;
static uint32_t g_max_irq_delay;
static uint32_t g_min_irq_delay;
static void *g_coherent_ptr_4k;
static dma_addr_t g_dma_handle_4k;

/* common across both devices */
struct adc_dma_xx {
	struct semaphore sem;
	struct device *pdev_dev;
};

static struct adc_dma_xx the_adc_dma_xx = {
	/*
	   .sem = initialize this at runtime before it is needed
	 */
	.pdev_dev = NULL,
};

static inline uint32_t get_dma_fill_level(void)
{
	uint32_t dma_fill_level;
	uint32_t dma_read_level;
	uint32_t dma_write_level;
	uint32_t dma_max_level;

	dma_fill_level = ioread32(g_ioremap_csr_addr +
				  CSR_DESCRIPTOR_FILL_LEVEL_REG);

	dma_read_level = dma_fill_level;
	dma_read_level &= ALTERA_MSGDMA_CSR_READ_FILL_LEVEL_MASK;
	dma_read_level >>= ALTERA_MSGDMA_CSR_READ_FILL_LEVEL_OFFSET;

	dma_write_level = dma_fill_level;
	dma_write_level &= ALTERA_MSGDMA_CSR_WRITE_FILL_LEVEL_MASK;
	dma_write_level >>= ALTERA_MSGDMA_CSR_WRITE_FILL_LEVEL_OFFSET;

	dma_max_level = (dma_write_level > dma_read_level) ?
	    (dma_write_level) : (dma_read_level);

	return dma_max_level;
}

static inline uint32_t get_dma_busy(void)
{
	uint32_t dma_status;

	dma_status = ioread32(g_ioremap_csr_addr + CSR_STATUS_REG);
	dma_status &= ALTERA_MSGDMA_CSR_BUSY_MASK;
	return dma_status;
}

static ssize_t adc_dma_read_block(char *buffer, size_t count)
{
	struct adc_dma_xx *dev = &the_adc_dma_xx;
	dma_addr_t dma_handle = 0;

	if (down_interruptible(&dev->sem)) {
		pr_info("adc_dma_st_read sem interrupted exit\n");
		return -ERESTARTSYS;
	}

	dma_handle = dma_map_single(dev->pdev_dev,
				    buffer,
				    count*2, DMA_FROM_DEVICE);

	if (dma_mapping_error(dev->pdev_dev, dma_handle)) {
		up(&dev->sem);
		pr_info("adc_dma_st_read dma mapping error exit\n");
		return -EBUSY;
	}

	iowrite32(0,
			g_ioremap_desc_addr + DESC_READ_ADDRESS_REG);
	iowrite32(dma_handle,
			g_ioremap_desc_addr + DESC_WRITE_ADDRESS_REG);
	iowrite32(count*2,
			g_ioremap_desc_addr + DESC_LENGTH_REG);
	iowrite32(START_DMA_MASK,
			g_ioremap_desc_addr + DESC_CONTROL_REG);

	while (get_dma_busy() != 0) {
		if (wait_event_interruptible(g_irq_wait_queue, (get_dma_busy() == 0))) {
			up(&dev->sem);
			pr_info
			("adc_dma_st_read wait interrupted exit\n");
			return -ERESTARTSYS;
		}
	}

	while (get_dma_fill_level() > 0) {
		if (wait_event_interruptible(g_irq_wait_queue, (get_dma_fill_level() ==	0))) {
			up(&dev->sem);
			pr_info
			("adc_dma_st_read wait interrupted exit\n");
			return -ERESTARTSYS;
		}
	}

	dma_unmap_single(dev->pdev_dev, dma_handle,
			count*2, DMA_FROM_DEVICE);

	up(&dev->sem);
	return count;
}

/* misc device - adc_dma */

static ssize_t adc_dma_co_read(struct file *fp, char __user *user_buffer,
				size_t count, loff_t *offset)
{
	struct adc_dma_xx *dev = &the_adc_dma_xx;
	size_t temp_user_count;
	size_t temp_dma_count;
	uint32_t next_dma_io_buf_ofst;
	uint32_t next_user_io_buf_ofst;
	int this_loop_count;

	if (down_interruptible(&dev->sem)) {
		pr_info("adc_dma_co_read sem interrupted exit\n");
		return -ERESTARTSYS;
	}

	if ((count*2) & DMA_DEVICE_MIN_BLOCK_MASK) {
		up(&dev->sem);
		pr_info("adc_dma_co_read count not min multiple exit\n");
		return -EINVAL;
	}

	this_loop_count = DMA_DEVICE_MIN_BLOCK_SIZE;
	temp_user_count = count*2;
	temp_dma_count = count*2;
	next_dma_io_buf_ofst = 0;
	next_user_io_buf_ofst = 0;

	iowrite32(0,
		g_ioremap_desc_addr + DESC_READ_ADDRESS_REG);
	iowrite32(g_dma_handle_4k + next_dma_io_buf_ofst,
		g_ioremap_desc_addr + DESC_WRITE_ADDRESS_REG);
	iowrite32(this_loop_count,
		g_ioremap_desc_addr + DESC_LENGTH_REG);
	iowrite32(START_DMA_MASK,
		g_ioremap_desc_addr + DESC_CONTROL_REG);

	temp_dma_count -= this_loop_count;
	next_dma_io_buf_ofst += this_loop_count;
	if (next_dma_io_buf_ofst >= PAGE_SIZE)
		next_dma_io_buf_ofst = 0;

	while (temp_user_count > 0) {
		if (temp_dma_count > 0) {
			iowrite32(0,
				  g_ioremap_desc_addr + DESC_READ_ADDRESS_REG);
			iowrite32(g_dma_handle_4k + next_dma_io_buf_ofst,
				  g_ioremap_desc_addr + DESC_WRITE_ADDRESS_REG);
			iowrite32(this_loop_count,
				  g_ioremap_desc_addr + DESC_LENGTH_REG);
			iowrite32(START_DMA_MASK,
				  g_ioremap_desc_addr + DESC_CONTROL_REG);

			temp_dma_count -= this_loop_count;
			next_dma_io_buf_ofst += this_loop_count;
			if (next_dma_io_buf_ofst >= PAGE_SIZE)
				next_dma_io_buf_ofst = 0;
		} else {
			while (get_dma_busy() != 0) {
				if (wait_event_interruptible(g_irq_wait_queue, (get_dma_busy() == 0))) {
					up(&dev->sem);
					pr_info
				   ("adc_dma_co_read wait interrupted exit\n");
					return -ERESTARTSYS;
				}
			}
		}

		while (get_dma_fill_level() > 0) {
			if (wait_event_interruptible(g_irq_wait_queue, (get_dma_fill_level() == 0))) {
				up(&dev->sem);
				pr_info
				   ("adc_dma_co_read wait interrupted exit\n");
				return -ERESTARTSYS;
			}
		}

		if (copy_to_user(user_buffer, g_coherent_ptr_4k +
				 next_user_io_buf_ofst, this_loop_count)) {
			up(&dev->sem);
			pr_info("adc_dma_co_read copy_to_user exit\n");
			return -EFAULT;
		}
		temp_user_count -= this_loop_count;


		user_buffer += this_loop_count;
		next_user_io_buf_ofst += this_loop_count;
		if (next_user_io_buf_ofst >= PAGE_SIZE)
			next_user_io_buf_ofst = 0;
	}

	up(&dev->sem);
	return count;
}

static const struct file_operations adc_dma_co_fops = {
	.owner = THIS_MODULE,
	.read = adc_dma_co_read,
//	.write = not used,
//	.llseek = not used,
};

static struct miscdevice adc_dma_co_device = {
	.minor = MISC_DYNAMIC_MINOR,
	.name = "adc_dma_co",
	.fops = &adc_dma_co_fops,
};

/* platform driver */
irqreturn_t adc_driver_interrupt_handler(int irq, void *dev_id)
{
	spin_lock(&g_irq_lock);

	/* clear the IRQ state */
	iowrite32(ALTERA_MSGDMA_CSR_IRQ_SET_MASK,
		  g_ioremap_csr_addr + CSR_STATUS_REG);

	spin_unlock(&g_irq_lock);
	wake_up_interruptible(&g_irq_wait_queue);

	return IRQ_HANDLED;
}

static int platform_probe(struct platform_device *pdev)
{
	int ret_val;
	struct resource *r;
	struct resource *msgdma_csr_mem_region;
	struct resource *msgdma_desc_mem_region;
	int irq;
	uint32_t dma_status;
	uint32_t dma_control;

	ret_val = -EBUSY;

	/* acquire the probe lock */
	if (down_interruptible(&g_dev_probe_sem))
		return -ERESTARTSYS;

	if (g_platform_probe_flag != 0)
		goto bad_exit_return;

	ret_val = -EINVAL;

	/* get our csr memory resource */
	r = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (r == NULL) {
		pr_err("IORESOURCE_MEM, 0 does not exist\n");
		goto bad_exit_return;
	}

	g_msgdma_csr_addr = r->start;
	g_msgdma_csr_size = resource_size(r);

	ret_val = -EBUSY;

	/* reserve our csr memory region */
	msgdma_csr_mem_region = request_mem_region(g_msgdma_csr_addr,
						     g_msgdma_csr_size,
						     "adc_dma_csr_hw_region");
	if (msgdma_csr_mem_region == NULL) {
		pr_err("request_mem_region failed: g_msgdma_csr_addr\n");
		goto bad_exit_return;
	}
	/* ioremap our csr memory region */
	g_ioremap_csr_addr = ioremap(g_msgdma_csr_addr, g_msgdma_csr_size);
	if (g_ioremap_csr_addr == NULL) {
		pr_err("ioremap failed: g_msgdma_csr_addr\n");
		goto bad_exit_release_mem_region_csr;
	}
	/* initialize the DMA controller */
	dma_status = ioread32(g_ioremap_csr_addr + CSR_STATUS_REG);
	if ((dma_status & (ALTERA_MSGDMA_CSR_BUSY_MASK |
			   ALTERA_MSGDMA_CSR_STOP_STATE_MASK |
			   ALTERA_MSGDMA_CSR_RESET_STATE_MASK |
			   ALTERA_MSGDMA_CSR_IRQ_SET_MASK)) != 0) {
		pr_err("initial dma status set unexpected: 0x%08X\n",
		       dma_status);
		goto bad_exit_release_mem_region_csr;
	}

	if ((dma_status & ALTERA_MSGDMA_CSR_DESCRIPTOR_BUFFER_EMPTY_MASK) == 0) {
		pr_err("initial dma status cleared unexpected: 0x%08X\n",
		       dma_status);
		goto bad_exit_release_mem_region_csr;
	}

	dma_control = ioread32(g_ioremap_csr_addr + CSR_CONTROL_REG);
	if ((dma_control & (ALTERA_MSGDMA_CSR_STOP_MASK |
			    ALTERA_MSGDMA_CSR_RESET_MASK |
			    ALTERA_MSGDMA_CSR_STOP_ON_ERROR_MASK |
			    ALTERA_MSGDMA_CSR_STOP_ON_EARLY_TERMINATION_MASK |
			    ALTERA_MSGDMA_CSR_GLOBAL_INTERRUPT_MASK |
			    ALTERA_MSGDMA_CSR_STOP_DESCRIPTORS_MASK)) != 0) {
		pr_err("initial dma control set unexpected: 0x%08X\n",
		       (uint32_t)dma_control);
		goto bad_exit_release_mem_region_csr;
	}

	ret_val = -EINVAL;

	/* Reset de DMA Controller */
	iowrite32(ALTERA_MSGDMA_CSR_RESET_MASK,
		  g_ioremap_csr_addr + CSR_CONTROL_REG);

	do{
		dma_status = ioread32(g_ioremap_csr_addr + CSR_STATUS_REG);
		dma_status &= ALTERA_MSGDMA_CSR_RESET_STATE_MASK;
	} while (dma_status != 0);


	/* get our desc memory resource */
	r = platform_get_resource(pdev, IORESOURCE_MEM, 1);
	if (r == NULL) {
		pr_err("IORESOURCE_MEM, 1 does not exist\n");
		goto bad_exit_iounmap_csr;
	}
	g_msgdma_desc_addr = r->start;
	g_msgdma_desc_size = resource_size(r);

	ret_val = -EBUSY;

	/* reserve our desc memory region */
	msgdma_desc_mem_region = request_mem_region(g_msgdma_desc_addr,
						      g_msgdma_desc_size,
						      "msgdma_desc_hw_region");
	if (msgdma_desc_mem_region == NULL) {
		pr_err("request_mem_region failed: g_msgdma_desc_addr\n");
		goto bad_exit_iounmap_csr;
	}
	/* ioremap our desc memory region */
	g_ioremap_desc_addr =
	    ioremap(g_msgdma_desc_addr, g_msgdma_desc_size);
	if (g_ioremap_desc_addr == NULL) {
		pr_err("ioremap failed: g_msgdma_desc_addr\n");
		goto bad_exit_release_mem_region_desc;
	}
	/* get our interrupt resource */
	irq = platform_get_irq(pdev, 0);
	if (irq < 0) {
		pr_err("irq not available\n");
		goto bad_exit_iounmap_desc;
	}
	g_msgdma_irq = irq;

	/* allocate some memory buffers */
	the_adc_dma_xx.pdev_dev = &pdev->dev;
	g_coherent_ptr_4k =
	    dma_alloc_coherent(the_adc_dma_xx.pdev_dev, PAGE_SIZE,
			       &g_dma_handle_4k, GFP_KERNEL);
	if (g_coherent_ptr_4k == NULL) {
		pr_err("dma_alloc_coherent failed 4KB\n");
		goto bad_exit_iounmap_desc;
	}

	/* register our interrupt handler */
	init_waitqueue_head(&g_irq_wait_queue);
	spin_lock_init(&g_irq_lock);
	g_irq_count = 0;
	g_max_irq_delay = 0;
	g_min_irq_delay = 0xFFFFFFFF;

	ret_val = request_irq(g_msgdma_irq,
			      adc_driver_interrupt_handler,
			      0,
			      the_platform_driver.driver.name,
			      &the_platform_driver);

	if (ret_val) {
		pr_err("request_irq failed");
		goto bad_exit_dma_free_coherent_4k;
	}
	/* enable the DMA global IRQ mask */
	iowrite32(ALTERA_MSGDMA_CSR_GLOBAL_INTERRUPT_MASK,
		  g_ioremap_csr_addr + CSR_CONTROL_REG);

	/* register misc device adc_dma_co */
	sema_init(&the_adc_dma_xx.sem, 1);
	ret_val = misc_register(&adc_dma_co_device);
	if (ret_val != 0) {
		pr_warn("Could not register device \"adc_dma_co\"...");
		goto bad_exit_freeirq;
	}

	g_platform_probe_flag = 1;
	up(&g_dev_probe_sem);
	pr_info("msgdma device successfully connected\n");

	return 0;

bad_exit_freeirq:
	free_irq(g_msgdma_irq, &the_platform_driver);
bad_exit_dma_free_coherent_4k:
	dma_free_coherent(the_adc_dma_xx.pdev_dev, PAGE_SIZE,
			  g_coherent_ptr_4k, g_dma_handle_4k);
bad_exit_iounmap_desc:
	iounmap(g_ioremap_desc_addr);
bad_exit_release_mem_region_desc:
	release_mem_region(g_msgdma_desc_addr, g_msgdma_desc_size);
bad_exit_iounmap_csr:
	iounmap(g_ioremap_csr_addr);
bad_exit_release_mem_region_csr:
	release_mem_region(g_msgdma_csr_addr, g_msgdma_csr_size);
bad_exit_return:
	up(&g_dev_probe_sem);
	return ret_val;
}

static int platform_remove(struct platform_device *pdev)
{
	misc_deregister(&adc_dma_co_device);

	/* disable the DMA global IRQ mask */
	iowrite32(0, g_ioremap_csr_addr + CSR_CONTROL_REG);

	free_irq(g_msgdma_irq, &the_platform_driver);
	dma_free_coherent(the_adc_dma_xx.pdev_dev, PAGE_SIZE,
			  g_coherent_ptr_4k, g_dma_handle_4k);
	iounmap(g_ioremap_desc_addr);
	release_mem_region(g_msgdma_desc_addr, g_msgdma_desc_size);
	iounmap(g_ioremap_csr_addr);
	release_mem_region(g_msgdma_csr_addr, g_msgdma_csr_size);

	if (down_interruptible(&g_dev_probe_sem))
		return -ERESTARTSYS;

	g_platform_probe_flag = 0;
	up(&g_dev_probe_sem);

	pr_info("msgdma device successfully removed\n");

	return 0;
}

static struct of_device_id msgdma_driver_dt_ids[] = {
	{
	 .compatible = "msgdma,driver-1.0"},
	{ /* end of table */ }
};

MODULE_DEVICE_TABLE(of, msgdma_driver_dt_ids);

static struct platform_driver the_platform_driver = {
	.probe = platform_probe,
	.remove = platform_remove,
	.driver = {
		   .name = "msgdma_driver",
		   .owner = THIS_MODULE,
		   .of_match_table = msgdma_driver_dt_ids,
		   },
};

static int msgdma_init(void)
{
	int ret_val;

	sema_init(&g_dev_probe_sem, 1);

	ret_val = platform_driver_register(&the_platform_driver);
	if (ret_val != 0) {
		pr_err("platform_driver_register returned %d\n", ret_val);
		return ret_val;
	}

	pr_info("msgdma module successfully inserted\n");
	return 0;
}

static void msgdma_exit(void)
{
	platform_driver_unregister(&the_platform_driver);
	pr_info("msgdma module successfully removed\n");
}

module_init(msgdma_init);
module_exit(msgdma_exit);

EXPORT_SYMBOL(adc_dma_read_block);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Antonio Carpeño <antonio.cruiz@upm.es>");
MODULE_AUTHOR("Mariano Ruiz <mariano.ruiz@upm.es>");
MODULE_DESCRIPTION("DMA example to transfer data from the adc to memory");
MODULE_VERSION("1.0");
