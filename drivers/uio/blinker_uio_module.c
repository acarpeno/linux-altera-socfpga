#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/clk.h>
#include <linux/io.h>
#include <linux/interrupt.h>
#include <linux/uio_driver.h>

#include "blinker_module.h"

/* global variables */
static struct semaphore g_dev_probe_sem;
static int g_platform_probe_flag;
static int g_blinker_driver_base_addr;
static int g_blinker_driver_size;
static int g_blinker_driver_irq;
static unsigned long g_blinker_driver_clk_rate;
static void *g_ioremap_addr;
static struct semaphore g_blinker_uio_dev_sem;
static struct platform_driver blinker_platform_driver;

/******************************************************************************
 	 	UIO DRIVER
		IRQ HANDLER, OPEN, RELEASE METHODS
******************************************************************************/

irqreturn_t blinker_uio_interrupt_handler(int irq, struct uio_info *dev_info)
{
	/* clear the interrupt */
	ioread8(IOADDR_BLINKER_STATE(g_ioremap_addr));

	return IRQ_HANDLED;
}

static int blinker_uio_irqcontrol(struct uio_info *info, s32 irq_on)
{
	u8 current_config;

	if (irq_on) {
		/* Enable interrupt  */
		current_config = ioread8(IOADDR_BLINKER_STATE(g_ioremap_addr));
		iowrite8((current_config & BLINKER_CONFIG_MSK) | BLINKER_IENA_MSK, IOADDR_BLINKER_CONFIG(g_ioremap_addr));
	} else {
		/* Disable interrupt  */
		current_config = ioread8(IOADDR_BLINKER_STATE(g_ioremap_addr));
		iowrite8((current_config & BLINKER_CONFIG_MSK) & BLINKER_IDIS_MSK, IOADDR_BLINKER_CONFIG(g_ioremap_addr));
		/* ensure there is no pending IRQ */
		ioread8(IOADDR_BLINKER_STATE(g_ioremap_addr));
	}
	return 0;
}

static int blinker_uio_open(struct uio_info *info, struct inode *inode)
{
	if (down_trylock(&g_blinker_uio_dev_sem) != 0)
		return -EAGAIN;

	return 0;
}

static int blinker_uio_release(struct uio_info *info, struct inode *inode)
{

	/* ensure there is no pending IRQ */
	ioread8(IOADDR_BLINKER_STATE(g_ioremap_addr));

	up(&g_blinker_uio_dev_sem);
	return 0;
}

static struct uio_info blinker_uio_info = {
	.name = "blinker_uio_module",
	.version = "1.0",
	.irq_flags = 0,
	.handler = blinker_uio_interrupt_handler,
	.open = blinker_uio_open,
	.release = blinker_uio_release,
	.irqcontrol = blinker_uio_irqcontrol,
};


/******************************************************************************
 	 	PLATFORM-DEVICE DRIVER
		PROBE, REMOVE, RELEASE, INIT AND EXIT METHODS
******************************************************************************/

static int platform_probe(struct platform_device *pdev)
{
	int ret_val;
	struct resource *r;
	struct resource *blinker_driver_mem_region;
	int irq;
	struct clk *clk;
	unsigned long clk_rate;
	int i;

	ret_val = -EBUSY;

	/* acquire the probe lock */
	if (down_interruptible(&g_dev_probe_sem))
		return -ERESTARTSYS;

	if (g_platform_probe_flag != 0)
		goto bad_exit_return;

	ret_val = -EINVAL;

	/* get memory resource */
	r = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (r == NULL) {
		pr_err("IORESOURCE_MEM, 0 does not exist\n");
		goto bad_exit_return;
	}

	g_blinker_driver_base_addr = r->start;
	g_blinker_driver_size = resource_size(r);

	/* get interrupt resource */
	irq = platform_get_irq(pdev, 0);
	if (irq < 0) {
		pr_err("irq not available\n");
		goto bad_exit_return;
	}

	g_blinker_driver_irq = irq;

	/* get clock resource */
	clk = clk_get(&pdev->dev, NULL);
	if (IS_ERR(clk)) {
		pr_err("clk not available\n");
		goto bad_exit_return;
	} else {
		clk_rate = clk_get_rate(clk);
	}

	g_blinker_driver_clk_rate = clk_rate;

	ret_val = -EBUSY;

	/* reserve memory region */
	blinker_driver_mem_region = request_mem_region(g_blinker_driver_base_addr,
						    g_blinker_driver_size,
						    "blinker_uio_driver_hw_region");
	if (blinker_driver_mem_region == NULL) {
		pr_err("request_mem_region failed: g_blinker_driver_base_addr\n");
		goto bad_exit_return;
	}

	/* ioremap memory region */
	g_ioremap_addr = ioremap(g_blinker_driver_base_addr, g_blinker_driver_size);
	if (g_ioremap_addr == NULL) {
		pr_err("ioremap failed: g_blinker_driver_base_addr\n");
		goto bad_exit_release_mem_region;
	}

	/* initialize uio_info struct uio_mem array */
	blinker_uio_info.mem[0].memtype = UIO_MEM_PHYS;
	blinker_uio_info.mem[0].addr = r->start;
	if (resource_size(r) > PAGE_SIZE) blinker_uio_info.mem[0].size = resource_size(r);
	else blinker_uio_info.mem[0].size = PAGE_SIZE;
	blinker_uio_info.mem[0].name = "blinker_uio_driver_hw_region";
	blinker_uio_info.mem[0].internal_addr = g_ioremap_addr;

	for (i = 1; i < MAX_UIO_MAPS; i++)
		blinker_uio_info.mem[i].size = 0;

	/* initialize uio_info irq */
	blinker_uio_info.irq = g_blinker_driver_irq;

	/* register the uio device */
	sema_init(&g_blinker_uio_dev_sem, 1);
	ret_val = uio_register_device(&pdev->dev, &blinker_uio_info);
	if (ret_val != 0) {
		pr_warn("Could not register device \"blinker_uio\"...");
		goto bad_exit_iounmap;
	}

	g_platform_probe_flag = 1;
	up(&g_dev_probe_sem);
	return 0;

bad_exit_iounmap:
	iounmap(g_ioremap_addr);
bad_exit_release_mem_region:
	release_mem_region(g_blinker_driver_base_addr, g_blinker_driver_size);
bad_exit_return:
	up(&g_dev_probe_sem);
	pr_info("platform_probe bad_exit\n");
	return ret_val;
}

static int platform_remove(struct platform_device *pdev)
{
	uio_unregister_device(&blinker_uio_info);

	iounmap(g_ioremap_addr);
	release_mem_region(g_blinker_driver_base_addr, g_blinker_driver_size);

	if (down_interruptible(&g_dev_probe_sem))
		return -ERESTARTSYS;

	g_platform_probe_flag = 0;
	up(&g_dev_probe_sem);

	return 0;
}

static struct of_device_id blinker_driver_dt_ids[] = {
	{
	 .compatible = "blinker,driver-1.0"},
	{ /* end of table */ }
};

MODULE_DEVICE_TABLE(of, blinker_driver_dt_ids);

static struct platform_driver blinker_platform_driver = {
	.probe = platform_probe,
	.remove = platform_remove,
	.driver = {
		   .name = "blinker",
		   .owner = THIS_MODULE,
		   .of_match_table = blinker_driver_dt_ids,
		   },
};

static int blinker_init(void)
{
	int ret_val;

	sema_init(&g_dev_probe_sem, 1);

	ret_val = platform_driver_register(&blinker_platform_driver);
	if (ret_val != 0) {
		pr_err("blinker module insert FAILED.\n");
		return ret_val;
	}

	pr_info("blinker module successfully inserted\n");
	return 0;
}

static void blinker_exit(void)
{
	platform_driver_unregister(&blinker_platform_driver);

	pr_info("blinker module successfully removed\n");
}

module_init(blinker_init);
module_exit(blinker_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Antonio Carpeño <antonio.cruiz@upm.es>");
MODULE_AUTHOR("Mariano Ruiz <mariano.ruiz@upm.es>");
MODULE_DESCRIPTION("blinker peripheral uio_platform_driver example");
MODULE_VERSION("1.0");
