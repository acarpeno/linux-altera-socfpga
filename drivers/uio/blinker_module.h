#define BLINKER_BASE	0xff240000
#define BLINKER_SIZE 	PAGE_SIZE

//#include <io.h>
#define SYSTEM_BUS_WIDTH 						(8)
#define __IO_CALC_ADDRESS_NATIVE(BASE, REGNUM)	((void *)(((unsigned char*)BASE) + ((REGNUM) * (SYSTEM_BUS_WIDTH/8))))
#define IORD(BASE, REGNUM) 						*(unsigned long*)(__IO_CALC_ADDRESS_NATIVE ((BASE), (REGNUM)))
#define IOWR(BASE, REGNUM, DATA) 				*(unsigned long*)(__IO_CALC_ADDRESS_NATIVE ((BASE), (REGNUM))) = (DATA)

/* SPEED register */
#define BLINKER_SPEED_REG              	0
#define IOADDR_BLINKER_SPEED(base) 		__IO_CALC_ADDRESS_NATIVE(base, BLINKER_SPEED_REG)
#define IOWR_BLINKER_SPEED(base, data)	IOWR(base, BLINKER_SPEED_REG, data)
#define BLINKER_SPEED_MSK           	(0x0F)

/* CONFIG register */
#define BLINKER_CONFIG_REG              1
#define IOADDR_BLINKER_CONFIG(base)		__IO_CALC_ADDRESS_NATIVE(base, BLINKER_CONFIG_REG)
#define IOWR_BLINKER_CONFIG(base, data) IOWR(base, BLINKER_CONFIG_REG, data)
#define BLINKER_CONFIG_MSK         		(0x07)
#define BLINKER_START_MSK          		(0x01)
#define BLINKER_STOP_MSK           		(0xFE)
#define BLINKER_BLINK_MSK          		(0x02)
#define BLINKER_SHIFT_MSK          		(0xFD)
#define BLINKER_IENA_MSK           		(0x04)
#define BLINKER_IDIS_MSK           		(0xFB)

/* LEDS register */
#define BLINKER_LEDS_REG				0
#define IOADDR_BLINKER_LEDS(base)		__IO_CALC_ADDRESS_NATIVE(base, BLINKER_LEDS_REG)
#define IORD_BLINKER_LEDS(base)			IORD(base, BLINKER_LEDS_REG)
#define BLINKER_LEDS_MSK				(0x0F)

/* STATE register */
#define BLINKER_STATE_REG				1
#define IOADDR_BLINKER_STATE(base)		__IO_CALC_ADDRESS_NATIVE(base, BLINKER_STATE_REG)
#define IORD_BLINKER_STATE(base)		IORD(base, BLINKER_STATE_REG)
#define BLINKER_STATE_MSK				(0xFF)

/*
 * ioctl values
 */
#define OUR_IOC_TYPE		(0xEE)
#define IOC_SET_SPEED		_IOW(OUR_IOC_TYPE, 0, u8)
#define IOC_SET_RUNNING		_IO(OUR_IOC_TYPE, 1)
#define IOC_CLEAR_RUNNING	_IO(OUR_IOC_TYPE, 2)
#define IOC_ENA_INT			_IO(OUR_IOC_TYPE, 6)
#define IOC_DIS_INT			_IO(OUR_IOC_TYPE, 7)
#define IOC_SET_MODE		_IOW(OUR_IOC_TYPE, 3, u8)
#define IOC_GET_LEDS		_IOR(OUR_IOC_TYPE, 4, u8)
#define IOC_GET_CONFIG		_IOR(OUR_IOC_TYPE, 5, u8)

/*
 * ioctl values
 *
#define IOC_SET_SPEED		(0x4001EE00)
#define IOC_SET_MODE		(0x4001EE03)
#define IOC_SET_RUNNING		(0x0000EE01)
#define IOC_CLEAR_RUNNING	(0x0000EE02)
#define IOC_ENA_INT			(0x0000EE06)
#define IOC_DIS_INT			(0x0000EE07)
#define IOC_GET_LEDS     	(0x8001EE04)
#define IOC_GET_CONFIG     	(0x8001EE05)
 */


/*To decode a hex IOCTL code:

Most architectures use this generic format, but check
include/ARCH/ioctl.h for specifics, e.g. powerpc
uses 3 bits to encode read/write and 13 bits for size.

 bits    meaning
 31-30	00 - no parameters: uses _IO macro
		10 - read: _IOR
		01 - write: _IOW
		11 - read/write: _IOWR

 29-16	size of arguments

 15-8	ascii character supposedly unique to each driver

 7-0	function #


So for example 0x82187201 is a read with arg length of 0x218,
character 'r' function 1.
*/

